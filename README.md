# Salesforce IBMi (AS400) Reference Application #

This repository contains the working source code for bi-directional Salesforce to AS400 integration using Mulesoft runtime, Infoview AS400 connector, and Web Transaction Framework. 

Please refer to this article for more details: https://www.infoviewsystems.com/connect-ibm-i-as400-to-salesforce-with-mulesoft/

## Salesforce Setup ##

The only change in the Salesforce is to set up the custom order statuses:

Setup -> Orders -> Fields -> Status -> set up the following values in the pick list:

- Picked
- Shipped
- SentToERP
- Cancelled

## AS400 Setup ##

- Create new library MULEDEMOS and a source file DEMOSRC
- Transfer source members from the IBMi project to the source file and compile the components
- Request a trial version of Infoview's Web Transaction Framework or change EDTORDER program to implement the data queue write into DEMO12REQ and listen for Mule response in DEMO12RES data queues. 
- Access "ERP" demo screen by calling EDTORDER program

## Mulesoft setup ## 
The demo consists of two Mulesoft apps:
-	Salesforce System API – responsible for communicating with Salesforce, polling for new active orders, and updating the order status
-	AS400 System API – responsible for publishing new orders to ERP, and listening for order status change events

This is a simplified set up, in real life scenario it’s common to also provide the Process API layer that would be responsible for mapping, routing and orchestration.
1.	Import demo projects into the Studio
2.	Create new configuration and check both demo apps so they would be started at the same time

For trial licenses or any questions please contact Infoview Systems Inc at <mule400@infoviewsystems.com> or +1 (734) 293-2160. 
