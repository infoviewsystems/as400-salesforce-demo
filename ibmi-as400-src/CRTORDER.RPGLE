     H DEBUG DATFMT(*ISO)
     H*================================================================
     H*  C R E A T I O N     P A R A M E T E R S                      *
     H*CRT: CRTBNDRPG                DFTACTGRP(*NO) ACTGRP(TRNAG)   +:*
     H*CRT:  dbgview(*all) option(*nodebugio)                       +:*
     H*CRT:  PGMINFO(*PCML) INFOSTMF('/tmp/crtorder.pcml')         :*
     H*================================================================
     H AlwNull(*UsrCtl)
     forderHdr  if a e           k disk
     f                                     prefix(h_)
     forderDtl  if a e           k disk
     f                                     prefix(d_)

     D i               s             10i 0 inz(0)

     d main            pr                  extpgm('CRTORDER')
     d  srcOrder                      8p 0
     d  trgtOrder                     8p 0
     d  ordAmt                       10p 2
     d  ordSts                       10a
     d  nbrLines                      4p 0
     d    linesIn                          likeds(linesInFmt) dim(10)

     d linesInfmt      DS                  Dim(10) qualified
     d   itemNo                      10a
     d   qty                          5p 0
     d   price                       10p 2

     d main            pi
     d  srcOrder                      8p 0
     d  trgtOrder                     8p 0
     d  ordAmt                       10p 2
     d  ordSts                       10a
     D  nbrLines                      4p 0
     d   linesIn                           likeds(linesInfmt) dim(10)

      /free
        clear trgtOrder;
        // Create corresponding ERP order number
        trgtOrder = srcOrder + 10000;

        // Write order details to Order heaader file
        chain  trgtOrder orderHdr;
        if not %found();
           h_erpOrder  = trgtOrder;
           h_sfdcOrder = srcOrder;
           h_ordAmt    = ordAmt;
           h_ordSts    = ordSts;
           h_ordDt  = %date();
           h_custmr = 0;
           write  orderHdrr;

           // Write Item details ti Order Details file
           for i = 1 to nbrLines;
             d_erpOrder  = trgtOrder;
             d_itemNo    = linesIn(i).itemNo;
          // d_itmName   = %trim(linesIn(i).itemName);
             d_itmName   = *blanks;
             d_unitPrc   = linesIn(i).price;
             d_quantity  = linesIn(i).qty;
             d_totalPrc  = linesIn(i).qty * linesIn(i).price;
             write  orderDtlr;
           endfor;
        endIf;
        return; // *inlr = *on;
      /end-free

